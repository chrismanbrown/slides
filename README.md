# slides

these are some dang slide decks okay?

<https://chrismanbrown.gitlab.io/slides/>

## about

this repo represents a workflow for writing content in markdown and then using pandoc to convert it into a reveal.js HTML presentation.

it uses gitlab pages and gitlab CI

## how to

1. write your slides in markdown in `/src`. (Use pandoc markdown for slides.)
2. `make build`
2. push it (push it real good)

## todo

- [x] vendor reveal.js (cdn is sometimes real slow)
- [x] include build step in CI
- [ ] how do you run multiline bash commands in make? this is breaking my pipeline!

## resources

- <https://medium.com/isovera/devops-for-presentations-reveal-js-markdown-pandoc-gitlab-ci-34d07d2c1011>
