# show available commands
default:
  just --list --unsorted

# watch for changes
watch:
  fd . 'src/' -e md | entr make build
