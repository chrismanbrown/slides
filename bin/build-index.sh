#!/bin/zsh
cat templates/index.before.md > index.md

for file in $(ls -1 src/*.md | sort -r)
do
  dest="${$(basename $file)/md/html}"
  pandoc --template=templates/markdown.template --wrap=none --variable=file:$dest $file >> index.md
done
