SRCS := $(shell find src -name '*.md')
HTML := $(SRCS:src/%.md=%.html)
vpath %.md src
vpath %.html www

build: index vendor slides
	rsync -vurp images/* www/images
	pandoc -t html --standalone --wrap=none -o www/index.html index.md

slides: $(HTML)

vendor:
	rsync -vurp vendor/* www/reveal

%.html: %.md
	pandoc -t revealjs -s --incremental --wrap=none --slide-level=3 -o www/$@ $<

.PHONY: index
index:
	./bin/build-index.sh
