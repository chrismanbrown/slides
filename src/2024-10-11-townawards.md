---
title: TILDE TOWN AWARDS
description: the townies
date: Oct 11, 2024
theme: dracula
pdfSeparateFragments: false
---

## The Townies

2024 Tidle Town Awards

::: notes
hello and welcome
to the 2024 tilde town awards!
affectionately nicknamed
The Townies

i'm your host dozens
and i'm hear to read you categories and winners
determined by vilmibm

the categories are:
1) HOME DIRECTORIES,
2) SOCIAL, and
3) TOWNIES CHOICE
:::

## HOMEDIRS


### most bytes in home dir

- artemis

- 13G

### most files in home dir

- thyme

- 451,184 files

- the reason why is hilarious and i recommend looking

### most dot files

- vshih

### most .txt files

- endorphant

### most cursed home directory

- agafnd

- (see ~agafnd/cursed)

### biggest public\_html

- sose

### most secretive user

::: fragment
most 0600 files
:::

- mjbmr

- 23,386 secret files

### most executable files

- brighty

- 6,473 executables

- followed by minerobber at 4,781

- shout out to dzwdz in third at 3,803

### biggest gopher hole

- tomasino

- 74,491

- runner up fst at 28,177

### biggest gemini capsule

- vidak

- 163,821

- runner up maxine at 61,769

### coolest homedir

::: fragment
user whose home directory mentions the word "cool" the most 
:::

- vilmibm with 61,408

- (4x the runner up)

- runner up bear

### most feelings

(most lines written to feels)

- pawky

- 316,009 lines

- (That's 20x the runner up)

- runner up owl

- 14,403

### most haiku

- emiltayl

- see ~emiltayl/public\_html/haikus

### fewest files in home dir

- ninlenna and walfisch

- ZERO!

- runner up mcclung at 2

### longest filename

- extratone

- 225 characters

- 1st runnerup mischk

- 174 characters

## SOCIALS

### most bbj posts

- vilmibm

- 461 posts

- runner up dozens

- 432 posts

### most fucks given

- dzwdz

- 4303 fucks

## TOWNIES CHOICE

subjective awards

### best cadastre plot

- flowercorpse

- big shout out to people collaborating to draw out a river

### cutest username

- omg there were too many to read through

- i liked blanketritual though

### Fewest amount of "the"

- bx

### hardest working robot

- our

### cutest robot

- pinhook

### kindest robot

- kindrobot

### most likely to loudly cheer you on at a sporting event

- m455

### most likely to go into a dark scary basement on your behalf

- elly

### best mayor of town and bestest birthday boy

- vilmibm

- <3
