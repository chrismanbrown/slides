---
title: "strixhaven session zero"
description: "a prezi for my D&D group"
date: "Feb 17, 2022"
showNotes: true
revealjs-url: 'reveal'
---
## Agenda

<!--
To export these slides:
pandoc -t revealjs -s --slide-level=3 -o zero.html zero.md
//-->

1. Pitch
2. Logistics
3. Special Mechanics
4. Character Creation
5. World Building

::: notes
make your face friendly!
:::

## Pitch

Concept / Aim / Tone / Subject

😺😺😺

::: notes
it's an acrostic!

<https://200wordrpg.github.io/2016/supplement/2016/04/12/CATS.html>
:::

### Concept

::: notes
Strixhaven is the premiere magical learning institute in all of planespace. It is *not* Hogwarts. You are new students trying to juggle academic life, make new friends, and having adventures!
:::

### Aim

::: notes
- Graduate: Four years of school, 3 - 4 scenes per trimester
- Excel in your studies, make friends, and have adventures! Save the school from a mysterious threat.
:::

### Tone

::: notes
- Pretty light-hearted
- Pillars: Lots of social encounters, NPC interactions, and roleplay. Then combat, then exploration
:::

### Subject Matter

::: notes
Friendship and relationships. Loyalty and betrayal. Balancing academic life and personal life.
:::

## Logistics

Scheduling / Platforms

### Scheduling

- 📆 Monday and Thursday, 6pm
- ⏱ 1.5 - 2 hrs
- 1️⃣ 1st Mondays = 1 shot?

::: notes
To make sure we have a consensus
:::


### Platforms

- roll20
- dndbeyond?
- discord

::: notes
I'll buy the module for roll20

I *prefer* managing character sheets in ddb because i think they do a really good job and i find it convenient, but i don't want to have to buy they same module twice.

we'll do voice/video in discord.
:::


## Special Mechanics

🌈🦄✨

opt-in

### Extracurriculars

::: notes
Can have two

Gain student die (d4) in each of the clubs 2 associated skills. (Recover on long rest)

Example: Dragonchess Club (Deception, Investigation)

Gain a relationship.
:::

### Jobs

::: notes
Can have 1 (2 xtras, or 1 job + 1 extra)

Get money each week

Gain a relationship
:::

### Relationships

::: notes
Can have many

Can result in narrative banes or boons

Capstone = Inspiration (does stack)
:::


### Exams

::: notes
mechanics for studying, testing, and cheating

Can gain up to 2 student dice (d4) for each of the 2 skills covered by the exam (Recover on long rest)
:::

### Tracker

[https://ttm.sh/tdA](https://ttm.sh/tdA)


## Character Creation

Stats / Classes / Backgrounds / Colleges

<https://ttm.sh/iGn.pdf>

### Stats

::: notes
I think point buy is consistent, but I value consistency less than I value everybody having fun. So if we agree we want to roll stats, then I want us to do that instead.
:::

### Classes

::: notes
Every class has magical subclasses: Fighters can take Eldrich Knight or Arcane Archer. Rogues can take Arcane Trickster. Monks = 4 Elements

You don't even have to take a magical subclass if you don't want to! You can be here on an athletics scholarship or something if you want to be.

Because....
:::

### Backgrounds

::: notes
everyone should *probably* take the Strixhaven Student background, which gives you some spells and the Strixhaven Student feat, which gives you some free magic (2 cantrips and a 1st level spell)
:::

### Colleges

<https://ttm.sh/ick.pdf>

<div style="font-size:0.6em;">
| College     | Focus          | Conflict              | Mascot        | Spell              |
|-------------|----------------|-----------------------|---------------|--------------------|
| Silverquill | Eloquence      | Group vs. Individual  | Inkling       | Silvery Barbs      |
| Prismari    | Elemental Arts | Intellect vs. Emotion | Art Elemental | Kinetic Jaunt      |
| Witherbloom | Essence        | Growth vs. Decay      | Pest          | Wither and Bloom   |
| Lorehold    | Archaeomancy   | Order vs. Chaos       | Spirit Statue | Borrowed Knowledge |
| Quandrix    | Numeromancy    | Nature vs. Nurture    | Fractal       | Vortex Warp        |
</div>

::: notes
- Silverquill = basically good v evil
- Witherbloom = basicall life v death
:::


## World Building

### Pallete

| Yes                      | No                          |
|--------------------------|-----------------------------|
| magic (girls)            | torture                     |
| nemesis 4 hils           | assault                     |
| blue jeweled tarantula   | animal cruelty              |
|                          | mad sci-fi                  |
|                          | guns (no school shootings!) |
|                          | erotic role play (veils)    |

::: notes
To create a consensus, to create safety
:::

## END

😘

::: notes
Thanks for coming to my TED talk
:::

