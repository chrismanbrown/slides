---
title: CB's Slide Decks
---

## About

See: <https://gitlab.com/chrismanbrown/slides>

## Slides


- [TILDE TOWN AWARDS](2024-10-11-townawards.html): the townies (Oct 11, 2024)
- [feedback](2023-03-05-feedback.html): what it is and how to do it (March 5, 2023)
- [ed is the standard editor](2022-08-17-ed.html): how to ed (Aug 17, 2022)
- [strixhaven session zero](2022-02-17-zero.html): a prezi for my D&amp;D group (Feb 17, 2022)
- [What do you do here?](2019-04-03-purpose.html): Crafting a one-liner elevator pitch (April 3, 2019)
- [Elixir, Phoenix, Elm](2019-02-20-elixir.html): A Functional Programming stack (Feb 20, 2019)
- [Hello g38!](2017-03-17-g38.html): This is a lightning talk that I’m preparing for the three soon-to-graduate cohorts at Galvanize Platte Street (March 13, 2017)
